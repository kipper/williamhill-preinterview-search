Building the project
====================

The project can be built by running build.bat from the command line.

Running the tests
=================

The projects tests can be run by running buildandruntests.bat from the command line

Running the programme
=====================

Once the project is built there will be a search.bat and search.jar file placed in the target directory. You should make sure that search.jar
is in the same directory as search.bat. You should then be able to run the search.bat according to the original spec.

Next Steps
==========

Next steps for this project are to use matches to perform RegEx matching. Considerations to take are around any specific differences between POSIX
RegEx and the Java implementation. Implementing the p operator could be done by checking for the flag and adding an additional class which would
be delegated to from the FileSearcher file. This could be done in such a way that a variety of custom matchers were used and the existing
functionality extracted into a custom matcher class. I have not done this so far because for the level of implementation this would be overly
architected and introduce un-needed misdirection at this stage.

Source control
==============

I have used Source Control with this project to hopefully show how I have added to this project over time and also to include some level of
timestamping. I have used a private repo on gitlab, I am happy to provide read access if you like.