echo off

mkdir .\target
mkdir .\target\compiledclasses
copy .\src\main\resources\search.bat .\target\
javac -d .\target\compiledclasses -sourcepath .\src\main\java .\src\main\java\williamhill\search\runner\*.java .\src\main\java\williamhill\search\domain\*.java
jar cf .\target\search.jar -C .\target\compiledclasses\ .