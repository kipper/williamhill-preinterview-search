package williamhill.search.domain;

import java.util.*;

public class FileNames{
    
    private List<String> fileNamesList;
    
    public FileNames(){
        fileNamesList = new ArrayList<String>();
    }
    
    public int fileCount(){
        return fileNamesList.size();
    }
    
    public void addFile(String fileName){
        fileNamesList.add(fileName);
    }
    
    public void addAll(FileNames fileNames){
        fileNamesList.addAll(fileNames.fileNamesList);
    }
    
    @Override
    public String toString(){
        String files = "";
        for(String fileName : fileNamesList){
            files += fileName;
            files += "\n";
        }
        return files;
    }
}