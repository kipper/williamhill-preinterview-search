package williamhill.search.domain;

import java.io.*;

public class FileSearcher{
    
    private static final String SEPARATOR = "/";
    
    public FileNames findFiles(String nameToMatch, String rootDirectory){
        File file = new File(rootDirectory);
        FileNames fileNames = new FileNames();
        for(File child : file.listFiles()){
            addFileIfMatches(fileNames, child, nameToMatch, rootDirectory);
            processChildDirectories(fileNames, child, nameToMatch, rootDirectory);
        }
        
        return fileNames;
    }
    
    private void addFileIfMatches(FileNames fileNames, File file, String nameToMatch, String rootDirectory){
        if(file.getName().equals(nameToMatch)){
            fileNames.addFile(rootDirectory + SEPARATOR + file.getName());
        }
    }
    
    private void processChildDirectories(FileNames fileNames, File file, String nameToMatch, String rootDirectory){
        if(file.isDirectory() && file.canExecute()){
            FileNames childDirectoryFiles = findFiles(nameToMatch, rootDirectory + SEPARATOR + file.getName());
            fileNames.addAll(childDirectoryFiles);
        }
    }
}