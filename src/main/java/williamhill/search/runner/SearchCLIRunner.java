package williamhill.search.runner;

import williamhill.search.domain.*;

public class SearchCLIRunner{
    
    public static void main(String [] arguments){
        if(!arguments[0].equals("-f") || arguments.length != 3){
            System.out.println("Correct form is:\n    .\\search.bat -f <fileNameToSearch> <directoryToSearch>");
        } else {
            FileSearcher searcher = new FileSearcher();
            FileNames fileNames = searcher.findFiles(arguments[1], arguments[2]);
            System.out.println(fileNames);
        }
    }
}