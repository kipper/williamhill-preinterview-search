package williamhill.search.domain;

import org.junit.*;

import static org.junit.Assert.*;

public class FileSearcherTests{
    
    @Test
    public void testSearcherFindsFileInBaseDirectory(){
        String fileName = "test.xml";
        String rootDirectory = "searchtest";
        
        FileSearcher searcher = new FileSearcher();
        FileNames fileNames = searcher.findFiles(fileName, rootDirectory);
        
        assertEquals(1, fileNames.fileCount());
        assertEquals("searchtest/test.xml\n", fileNames.toString());
    }
    
    @Test
    public void testSearcherDoesntFindFileThatDoesntExistInDirectory(){
        String fileName = "notExists.txt";
        String rootDirectory = "searchtest";
        
        FileSearcher searcher = new FileSearcher();
        FileNames fileNames = searcher.findFiles(fileName, rootDirectory);
        
        assertEquals(0, fileNames.fileCount());
    }
    
    @Test
    public void testSearcherFindsFileInAChildDirectoryAndCorrectlyAppendsAllParentDirectoryNames(){
        String fileName = "pom.xml";
        String rootDirectory = "searchtest";
        
        FileSearcher searcher = new FileSearcher();
        FileNames fileNames = searcher.findFiles(fileName, rootDirectory);
        
        assertEquals(1, fileNames.fileCount());
        assertEquals("searchtest/subproject/pom.xml\n", fileNames.toString());
    }
    
    @Test
    public void testSearcherFindsMultipleFilesInMultipleChildDirectoriesAndCorrectlyAppendsParentDirectoryNames(){
        String fileName = "hello.txt";
        String rootDirectory = "searchtest";
        
        FileSearcher searcher = new FileSearcher();
        FileNames fileNames = searcher.findFiles(fileName, rootDirectory);
        
        assertEquals(2, fileNames.fileCount());
        assertTrue(fileNames.toString().contains("searchtest/subproject/hello.txt"));
        assertTrue(fileNames.toString().contains("searchtest/subproject2/src/example/hello.txt"));
    }
}