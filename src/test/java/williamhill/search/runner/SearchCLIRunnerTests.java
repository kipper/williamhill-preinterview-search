package williamhill.search.runner;

import java.io.*;

import org.junit.*;

import static org.junit.Assert.*;

public class SearchCLIRunnerTests{
    
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    
    @Before
    public void beforeEachTest(){
        System.setOut(new PrintStream(outContent));
    }

    @Test
    public void testRunnerReturnsCorrectStringWithNewLinesForFileInProvidedDirectory(){
        String [] arguments = {"-f", "test.xml", "searchtest"};
        SearchCLIRunner.main(arguments);
        assertTrue(outContent.toString().contains("searchtest/test.xml"));
    }
    
    @Test
    public void testRunnerGracefullyFailsIfArgumentsDoNotStartWithFlagF(){
        String [] arguments = {"hello", "searchtest", "test"};
        SearchCLIRunner.main(arguments);
        assertTrue(outContent.toString().contains("Correct form is:\n    .\\search.bat -f <fileNameToSearch> <directoryToSearch>"));
    }
    
    @Test
    public void testRunnerGracefullyFailsIfIncorrectNumberOfArguments(){
        String [] arguments = {"-f"};
        SearchCLIRunner.main(arguments);
        assertTrue(outContent.toString().contains("Correct form is:\n    .\\search.bat -f <fileNameToSearch> <directoryToSearch>"));
    }
}