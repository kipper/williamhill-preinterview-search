echo off

cmd /c .\build.bat


mkdir .\target
mkdir .\target\compiledtestclasses
javac -cp ".\lib\*;.\target\search.jar" -d .\target\compiledtestclasses -sourcepath .\src\test\java .\src\test\java\williamhill\search\runner\*.java .\src\test\java\williamhill\search\domain\*.java

java -cp ".\target\*;.\lib\*;.\target\compiledtestclasses" org.junit.runner.JUnitCore williamhill.search.runner.SearchCLIRunnerTests williamhill.search.domain.FileSearcherTests
